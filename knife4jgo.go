package knife4jgo

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/longyancang/knife4jgo/extend"
	"gitee.com/longyancang/knife4jgo/knife"
	"gitee.com/longyancang/knife4jgo/knife/img/icons"
	"gitee.com/longyancang/knife4jgo/knife/webjars/css"
	"gitee.com/longyancang/knife4jgo/knife/webjars/fonts"
	"gitee.com/longyancang/knife4jgo/knife/webjars/img"
	"gitee.com/longyancang/knife4jgo/knife/webjars/js"
	"gitee.com/longyancang/knife4jgo/knife/webjars/oauth"
	"gitee.com/longyancang/knife4jgo/openapi"
	"gitee.com/longyancang/knife4jgo/tonic"
	"github.com/gin-gonic/gin"
	"net/http"
	"path"
	"reflect"
	"runtime"
	"strings"
	"time"
)

func InitSwaggerKnife(router *gin.Engine, infos *openapi.Info) *Fizz {
	fizz := AddApiDocRouter(router, infos)
	knife.AddSwaggerResourcesRouter(router)
	knife.AddRouterOfDocHtml(router)
	icons.AddRouterOfAndroidChrome192x192Png(router)
	icons.AddRouterOfAndroidChrome512x512Png(router)
	icons.AddRouterOfAppleTouchIcon120x120Png(router)
	icons.AddRouterOfAppleTouchIcon152x152Png(router)
	icons.AddRouterOfAppleTouchIcon180x180Png(router)
	icons.AddRouterOfAppleTouchIcon60x60Png(router)
	icons.AddRouterOfAppleTouchIcon76x76Png(router)
	icons.AddRouterOfAppleTouchIconPng(router)
	icons.AddRouterOfFavicon16x16Png(router)
	icons.AddRouterOfFavicon32x32Png(router)
	icons.AddRouterOfMsapplicationIcon144x144Png(router)
	icons.AddRouterOfMstile150x150Png(router)
	icons.AddRouterOfSafariPinnedTabSvg(router)
	css.AddRouterOfAppAc23e017Css(router)
	css.AddRouterOfAppAc23e017CssGz(router)
	css.AddRouterOfChunk75464e7e8fb93ba5Css(router)
	css.AddRouterOfChunkD7d5f59cA9ffbfcbCss(router)
	css.AddRouterOfChunkVendorsF24a310aCss(router)
	css.AddRouterOfChunkVendorsF24a310aCssGz(router)
	fonts.AddRouterOfFontawesomeWebfont706450d7Ttf(router)
	fonts.AddRouterOfFontawesomeWebfont97493d3fWoff2(router)
	fonts.AddRouterOfFontawesomeWebfontD9ee23d5Woff(router)
	fonts.AddRouterOfFontawesomeWebfontF7c2b4b7Eot(router)
	fonts.AddRouterOfIconfont4ca3d0c0Ttf(router)
	fonts.AddRouterOfIconfontE2d2b98eEot(router)
	img.AddRouterOfEditormdLogo53ea80e2Svg(router)
	img.AddRouterOfFontawesomeWebfont29800836Svg(router)
	img.AddRouterOfIconfont1d48c203Svg(router)
	img.AddRouterOfLoadingC929501eGif(router)
	img.AddRouterOfLoading2x695405a9Gif(router)
	img.AddRouterOfLoading3x65eacf61Gif(router)
	js.AddRouterOfApp2fab4ac5Js(router)
	js.AddRouterOfApp2fab4ac5JsGz(router)
	js.AddRouterOfChunk069eb4372cfebf27Js(router)
	js.AddRouterOfChunk069eb4372cfebf27JsLICENSETxt(router)
	js.AddRouterOfChunk069eb4372cfebf27JsGz(router)
	js.AddRouterOfChunk0d102d5aB2bddffcJs(router)
	js.AddRouterOfChunk0d102d5aB2bddffcJsGz(router)
	js.AddRouterOfChunk0fd67716D57e2c41Js(router)
	js.AddRouterOfChunk0fd67716D57e2c41JsGz(router)
	js.AddRouterOfChunk260d712a390177feJs(router)
	js.AddRouterOfChunk260d712a390177feJsLICENSETxt(router)
	js.AddRouterOfChunk260d712a390177feJsGz(router)
	js.AddRouterOfChunk2d0af44e392afcd6Js(router)
	js.AddRouterOfChunk2d0bd799Eb48b7f1Js(router)
	js.AddRouterOfChunk2d0da532591ad7fcJs(router)
	js.AddRouterOfChunk3b888a658737ce4fJs(router)
	js.AddRouterOfChunk3b888a658737ce4fJsGz(router)
	js.AddRouterOfChunk3ec4aaa8A79d19f8Js(router)
	js.AddRouterOfChunk3ec4aaa8A79d19f8JsGz(router)
	js.AddRouterOfChunk589faee05bfd1708Js(router)
	js.AddRouterOfChunk589faee05bfd1708JsLICENSETxt(router)
	js.AddRouterOfChunk589faee05bfd1708JsGz(router)
	js.AddRouterOfChunk735c675c5b409314Js(router)
	js.AddRouterOfChunk735c675c5b409314JsGz(router)
	js.AddRouterOfChunk75464e7eB130271bJs(router)
	js.AddRouterOfChunk75464e7eB130271bJsGz(router)
	js.AddRouterOfChunkAdb9e9442c7f24feJs(router)
	js.AddRouterOfChunkAdb9e9442c7f24feJsLICENSETxt(router)
	js.AddRouterOfChunkAdb9e9442c7f24feJsGz(router)
	js.AddRouterOfChunkD7d5f59cE61130f3Js(router)
	js.AddRouterOfChunkD7d5f59cE61130f3JsLICENSETxt(router)
	js.AddRouterOfChunkD7d5f59cE61130f3JsGz(router)
	js.AddRouterOfChunkVendorsD51cf6f8Js(router)
	js.AddRouterOfChunkVendorsD51cf6f8JsLICENSETxt(router)
	js.AddRouterOfChunkVendorsD51cf6f8JsGz(router)
	oauth.AddRouterOfAxiosMinJs(router)
	oauth.AddRouterOfOauth2Html(router)
	return fizz
}

func AddApiDocRouter(router *gin.Engine, infos *openapi.Info) *Fizz {
	fizz := NewFromEngine(router)
	// Create a new route that serve the OpenAPI spec.
	fizz.GET(extend.ExtendsRegisterDefault.Cp.GetContextPath()+"/v3/api-docs", nil, fizz.OpenAPI(infos, "json"))
	return fizz
}

const ctxOpenAPIOperation = "_ctx_openapi_operation"

// Primitive type helpers.
var (
	Integer  int32
	Long     int64
	Float    float32
	Double   float64
	String   string
	Byte     []byte
	Binary   []byte
	Boolean  bool
	DateTime time.Time
)

// Fizz is an abstraction of a Gin engine that wraps the
// routes handlers with Tonic and generates an OpenAPI
// 3.0 specification from it.
type Fizz struct {
	gen    *openapi.Generator
	engine *gin.Engine
	*RouterGroup
}

// RouterGroup is an abstraction of a Gin router group.
type RouterGroup struct {
	group       *gin.RouterGroup
	gen         *openapi.Generator
	Name        string
	Description string
}

// New creates a new Fizz wrapper for
// a default Gin engine.
func New() *Fizz {
	return NewFromEngine(gin.New())
}

// NewFromEngine creates a new Fizz wrapper
// from an existing Gin engine.
func NewFromEngine(e *gin.Engine) *Fizz {
	// Create a new spec with the config
	// based on tonic internals.
	gen, _ := openapi.NewGenerator(
		&openapi.SpecGenConfig{
			ValidatorTag:      tonic.ValidationTag,
			PathLocationTag:   tonic.PathTag,
			QueryLocationTag:  tonic.QueryTag,
			HeaderLocationTag: tonic.HeaderTag,
			EnumTag:           tonic.EnumTag,
			DefaultTag:        tonic.DefaultTag,
		},
	)
	return &Fizz{
		engine: e,
		gen:    gen,
		RouterGroup: &RouterGroup{
			group: &e.RouterGroup,
			gen:   gen,
		},
	}
}

// ServeHTTP implements http.HandlerFunc for Fizz.
func (f *Fizz) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	f.engine.ServeHTTP(w, r)
}

// Engine returns the underlying Gin engine.
func (f *Fizz) Engine() *gin.Engine {
	return f.engine
}

// GinRouterGroup returns the underlying Gin router group.
func (f *Fizz) GinRouterGroup() *gin.RouterGroup {
	return f.group
}

// Generator returns the underlying OpenAPI generator.
func (f *Fizz) Generator() *openapi.Generator {
	return f.gen
}

// Errors returns the errors that may have occurred
// during the spec generation.
func (f *Fizz) Errors() []error {
	return f.gen.Errors()
}

// Group creates a new group of routes.
func (g *RouterGroup) Group(path, name, description string, handlers ...gin.HandlerFunc) *RouterGroup {
	// Create the tag in the specification
	// for this groups.
	g.gen.AddTag(name, description)

	return &RouterGroup{
		gen:         g.gen,
		group:       g.group.Group(path, handlers...),
		Name:        name,
		Description: description,
	}
}

// Use adds middleware to the group.
func (g *RouterGroup) Use(handlers ...gin.HandlerFunc) {
	g.group.Use(handlers...)
}

// GinRouterGroup returns the underlying Gin router group.
func (g *RouterGroup) GinRouterGroup() *gin.RouterGroup {
	return g.group
}

// GET is a shortcut to register a new handler with the GET method.
func (g *RouterGroup) GET(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "GET", infos, handlers...)
}

// POST is a shortcut to register a new handler with the POST method.
func (g *RouterGroup) POST(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "POST", infos, handlers...)
}

// PUT is a shortcut to register a new handler with the PUT method.
func (g *RouterGroup) PUT(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "PUT", infos, handlers...)
}

// PATCH is a shortcut to register a new handler with the PATCH method.
func (g *RouterGroup) PATCH(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "PATCH", infos, handlers...)
}

// DELETE is a shortcut to register a new handler with the DELETE method.
func (g *RouterGroup) DELETE(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "DELETE", infos, handlers...)
}

// OPTIONS is a shortcut to register a new handler with the OPTIONS method.
func (g *RouterGroup) OPTIONS(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "OPTIONS", infos, handlers...)
}

// HEAD is a shortcut to register a new handler with the HEAD method.
func (g *RouterGroup) HEAD(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "HEAD", infos, handlers...)
}

// TRACE is a shortcut to register a new handler with the TRACE method.
func (g *RouterGroup) TRACE(path string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	return g.Handle(path, "TRACE", infos, handlers...)
}

// Handle registers a new request handler that is wrapped
// with Tonic and documented in the OpenAPI specification.
func (g *RouterGroup) Handle(path, method string, infos []OperationOption, handlers ...gin.HandlerFunc) *RouterGroup {
	oi := &openapi.OperationInfo{}
	for _, info := range infos {
		info(oi)
	}
	type wrap struct {
		h gin.HandlerFunc
		r *tonic.Route
	}
	var wrapped []wrap

	// Find the handlers wrapped with Tonic.
	for _, h := range handlers {
		r, err := tonic.GetRouteByHandler(h)
		if err == nil {
			wrapped = append(wrapped, wrap{h: h, r: r})
		}
	}
	// Check that no more that one tonic-wrapped handler
	// is registered for this operation.
	if len(wrapped) > 1 {
		panic(fmt.Sprintf("multiple tonic-wrapped handler used for operation %s %s", method, path))
	}
	// If we have a tonic-wrapped handler, generate the
	// specification of this operation.
	if len(wrapped) == 1 {
		hfunc := wrapped[0].r

		// Set an operation ID if none is provided.
		if oi.ID == "" {
			oi.ID = hfunc.HandlerName()
		}
		oi.StatusCode = hfunc.GetDefaultStatusCode()

		// Set an input type if provided.
		it := hfunc.InputType()
		if oi.InputModel != nil {
			it = reflect.TypeOf(oi.InputModel)
		}

		// Consolidate path for OpenAPI spec.
		operationPath := joinPaths(g.group.BasePath(), path)

		// Add operation to the OpenAPI spec.
		operation, err := g.gen.AddOperation(operationPath, method, g.Name, it, hfunc.OutputType(), oi)
		if err != nil {
			panic(fmt.Sprintf(
				"error while generating OpenAPI spec on operation %s %s: %s",
				method, path, err,
			))
		}
		// If an operation was generated for the handler,
		// wrap the Tonic-wrapped handled with a closure
		// to inject it into the Gin context.
		if operation != nil {
			for i, h := range handlers {
				if funcEqual(h, wrapped[0].h) {
					orig := h // copy the original func
					handlers[i] = func(c *gin.Context) {
						c.Set(ctxOpenAPIOperation, operation)
						orig(c)
					}
				}
			}
		}
	}
	// Register the handlers with Gin underlying group.
	// 将每个处理器包裹起来
	handleFunc := extend.ExtendsRegisterDefault.Acwh.AssembleCustomWrapHandlerFunc(handlers)
	g.group.Handle(method, path, handleFunc...)

	return g
}

// OpenAPI returns a Gin HandlerFunc that serves
// the marshalled OpenAPI specification of the API.
func (f *Fizz) OpenAPI(info *openapi.Info, ct string) gin.HandlerFunc {
	f.gen.SetInfo(info)

	ct = strings.ToLower(ct)
	if ct == "" {
		ct = "json"
	}
	switch ct {
	case "json":
		return func(c *gin.Context) {
			api := f.gen.API()
			extend.ExtendsRegisterDefault.Cpdr.DuplicateRemoval(api)
			c.JSON(200, api)
		}
	case "yaml":
		return func(c *gin.Context) {
			api := f.gen.API()
			extend.ExtendsRegisterDefault.Cpdr.DuplicateRemoval(api)
			c.YAML(200, api)
		}
	}
	panic("invalid content type, use JSON or YAML")
}

// OperationOption represents an option-pattern function
// used to add informations to an operation.
type OperationOption func(*openapi.OperationInfo)

// StatusDescription sets the default status description of the operation.
func StatusDescription(desc string) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.StatusDescription = desc
	}
}

// Summary adds a summary to an operation.
func Summary(summary string) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Summary = summary
	}
}

// Summaryf adds a summary to an operation according
// to a format specifier.
func Summaryf(format string, a ...interface{}) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Summary = fmt.Sprintf(format, a...)
	}
}

// Description adds a description to an operation.
func Description(desc string) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Description = desc
	}
}

// Descriptionf adds a description to an operation
// according to a format specifier.
func Descriptionf(format string, a ...interface{}) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Description = fmt.Sprintf(format, a...)
	}
}

// ID overrides the operation ID.
func ID(id string) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.ID = id
	}
}

// Deprecated marks the operation as deprecated.
func Deprecated(deprecated bool) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Deprecated = deprecated
	}
}

// Response adds an additional response to the operation.
func Response(statusCode, desc string, model interface{}, headers []*openapi.ResponseHeader, example interface{}) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Responses = append(o.Responses, &openapi.OperationResponse{
			Code:        statusCode,
			Description: desc,
			Model:       model,
			Headers:     headers,
			Example:     example,
		})
	}
}

// ResponseWithExamples is a variant of Response that accept many examples.
func ResponseWithExamples(statusCode, desc string, model interface{}, headers []*openapi.ResponseHeader, examples map[string]interface{}) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Responses = append(o.Responses, &openapi.OperationResponse{
			Code:        statusCode,
			Description: desc,
			Model:       model,
			Headers:     headers,
			Examples:    examples,
		})
	}
}

// Header adds a header to the operation.
func Header(name, desc string, model interface{}) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Headers = append(o.Headers, &openapi.ResponseHeader{
			Name:        name,
			Description: desc,
			Model:       model,
		})
	}
}

// InputModel overrides the binding model of the operation.
func InputModel(model interface{}) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.InputModel = model
	}
}

// XCodeSample adds a code sample to the operation.
func XCodeSample(cs *openapi.XCodeSample) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.XCodeSamples = append(o.XCodeSamples, cs)
	}
}

// Overrides top-level security requirement for this operation.
// Note that this function can be used more than once to add several requirements.
func Security(security *openapi.SecurityRequirement) func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Security = append(o.Security, security)
	}
}

// Add an empty security requirement to this operation to make other security requirements optional.
func WithOptionalSecurity() func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		var emptyRequirement openapi.SecurityRequirement = make(openapi.SecurityRequirement)
		o.Security = append(o.Security, &emptyRequirement)
	}
}

// Remove any top-level security requirements for this operation.
func WithoutSecurity() func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.Security = []*openapi.SecurityRequirement{}
	}
}

// XInternal marks the operation as internal.
func XInternal() func(*openapi.OperationInfo) {
	return func(o *openapi.OperationInfo) {
		o.XInternal = true
	}
}

// OperationFromContext returns the OpenAPI operation from
// the given Gin context or an error if none is found.
func OperationFromContext(ctx context.Context) (*openapi.Operation, error) {
	if v := ctx.Value(ctxOpenAPIOperation); v != nil {
		if op, ok := v.(*openapi.Operation); ok {
			return op, nil
		}
		return nil, errors.New("invalid type: not an operation")
	}
	return nil, errors.New("operation not found")
}

func joinPaths(abs, rel string) string {
	if rel == "" {
		return abs
	}
	final := path.Join(abs, rel)
	as := lastChar(rel) == '/' && lastChar(final) != '/'
	if as {
		return final + "/"
	}
	return final
}

func lastChar(str string) uint8 {
	if str == "" {
		panic("empty string")
	}
	return str[len(str)-1]
}

func funcEqual(f1, f2 interface{}) bool {
	v1 := reflect.ValueOf(f1)
	v2 := reflect.ValueOf(f2)
	if v1.Kind() == reflect.Func && v2.Kind() == reflect.Func { // prevent panic on call to Pointer()
		return runtime.FuncForPC(v1.Pointer()).Entry() == runtime.FuncForPC(v2.Pointer()).Entry()
	}
	return false
}
