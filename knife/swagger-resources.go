package knife

import (
	"gitee.com/longyancang/knife4jgo/extend"
	"gitee.com/longyancang/knife4jgo/utils"
	"github.com/gin-gonic/gin"
)

const (
	SWAGGER_RESOURCES_CONFIG_PATH = "/v3/api-docs/swagger-config"
)

func AddSwaggerResourcesRouter(router *gin.Engine) {
	contextPath := extend.ExtendsRegisterDefault.Cp.GetContextPath()
	swaggerResourcesContent := `{"configUrl": "` + contextPath + `/v3/api-docs/swagger-config","oauth2RedirectUrl": "` + contextPath + `/swagger-ui/oauth2-redirect.html","url": "` + contextPath + `/v3/api-docs","validatorUrl": ""}`
	utils.GetJson(router, SWAGGER_RESOURCES_CONFIG_PATH, swaggerResourcesContent)
}
