参考资料：
https://gitee.com/youbeiwuhuan/knife4go?force_mobile=true
https://github.com/wI2L/fizz
https://github.com/loopfz/gadgeto/tree/master/tonic


1. example 包下为示例，直接执行即可

2. extend为扩展包，都可以重新自定义
   Cpdr ContextPathDuplicateRemoval // swagger查询api接口时，把根路径去掉，打开swagger页面时，url根路径会重复，如果没有重复可以不启用
   Vem  ValidatorErrorMessages      // 自定义Validator验证错误描述信息
   Cgwh CustomGinWrapHandler        // 自定义gin.handler包装器,默认为panic统一拦截器
   Acwh AssembleCustomWrapHandler   // 把每个gin.Handler包装一层自定义CustomGinWrapHandler
   Cver CustomValidatorErrorRes     // 自定义参数验证错误响应体,默认为自定义的Result结构体
   Cp   ContextPath                 // 根路径：http://localhost:8080/example/doc.html#  example为根路径

3. 使用方法跟上面参考资料一致，本项目只做了一些增强功能

4. 依赖导入命令 go get gitee.com/longyancang/knife4jgo