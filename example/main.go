package main

import (
	"fmt"
	"gitee.com/longyancang/knife4jgo"
	"gitee.com/longyancang/knife4jgo/extend"
	"gitee.com/longyancang/knife4jgo/openapi"
	"gitee.com/longyancang/knife4jgo/tonic"
	"gitee.com/longyancang/knife4jgo/utils/result"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func main() {
	// 设置日志环境日志级别
	gin.SetMode(gin.ReleaseMode)
	// 创建一个新的 Gin 引擎实例
	router := gin.New()
	// 扩展器注册
	extend.ExtendsRegisterDefaultInit("example")
	infos := &openapi.Info{
		Title:       "测试模块",
		Description: `测试专用模块`,
		Version:     "1.0.0",
	}
	fizz := knife4jgo.InitSwaggerKnife(router, infos)
	// 获取 contextPath
	contextPath := fizz.Group(extend.ExtendsRegisterDefault.Cp.GetContextPath(), "", "")

	group := contextPath.Group("/task", "任务", "Your daily dose of freshness，你的描述")
	group.POST("/FindTaskByIds", []knife4jgo.OperationOption{
		knife4jgo.Summary("任务查询"),
		knife4jgo.Descriptionf("接口描述"),
		knife4jgo.Response("400", "Bad request", nil, nil,
			map[string]interface{}{"error": "任务已存在"},
		),
	}, tonic.Handler(FindTaskEmail, http.StatusOK))

	group.GET("FindTestByName", []knife4jgo.OperationOption{
		knife4jgo.Summary("根据姓名查询"),
		knife4jgo.Response("400", "Bad request", nil, nil, nil),
	}, tonic.Handler(FindTestByName, 200))

	_ = router.Run(":8080")
}

func FindTaskEmail(c *gin.Context, params *TaskEmailReq) (*result.Result[*TaskEmailRes], error) {
	fmt.Println(params)
	return result.NewOkResult(&TaskEmailRes{Id: 1, SendTime: time.Now().UTC(), Interval: 2}, c), nil
}

func FindTestByName(c *gin.Context, params *TestReq) (*result.Result[*TestRes], error) {
	fmt.Println(params)
	return result.NewOkResult(&TestRes{Id: 1, CreateTime: time.Now().UTC(), Name: params.Name}, c), nil
}

// TaskEmailReq errorMsg 为 validate tag 自定义，对应ValidatorErrorMessages接口
type TaskEmailReq struct {
	Id            int       `json:"id" validate:"required" description:"主键ID" example:"1"`
	Interval      int       `json:"interval" validate:"required,gt=12" description:"间隔时间" example:"1" errorMsg:"必须大于12"`
	SendTime      time.Time `json:"sendTime" description:"发送执行时间"`
	Authorization string    `header:"Authorization" validate:"required" description:"token请求头"`
}

type TaskEmailRes struct {
	Id       int       `json:"id,omitempty" description:"任务id"`
	SendTime time.Time `json:"sendTime,omitempty" description:"发送执行时间"`
	Interval int       `json:"interval,omitempty" description:"发送时间间隔"`
}

type TestReq struct {
	Name          string `json:"name" query:"name"  validate:"required" description:"姓名" example:"小猫" errorMsg:"姓名不能为空"`
	Authorization string `header:"Authorization" validate:"required" description:"token请求头"`
}

type TestRes struct {
	Id         int       `json:"id,omitempty" description:"主键"`
	Name       string    `json:"name" validate:"required" description:"姓名"`
	CreateTime time.Time `json:"createTime,omitempty" description:"创建时间"`
}
