package extend

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"reflect"
	"strings"
)

type ValidatorErrorMessages interface {
	ErrorMessagesFunc(u interface{}, err error) string
}

type errorMessages struct {
	isEnable bool // 是否启用
}

var _ ValidatorErrorMessages = &errorMessages{}

func (e *errorMessages) ErrorMessagesFunc(u interface{}, err error) string {
	if !e.isEnable { // 不启用直接返回
		return err.Error()
	}
	if err == nil { //如果为nil 说明校验通过
		return ""
	}
	_, ok := err.(*validator.InvalidValidationError) //如果是输入参数无效，直接返回原来异常描述
	if ok {
		return err.Error()
	}
	validationErrs := err.(validator.ValidationErrors) //断言是ValidationErrors
	builder := strings.Builder{}
	// 处理验证异常的属性
	for _, validationErr := range validationErrs {
		fieldName := validationErr.Field() //获取是哪个字段不符合格式
		typeOf := reflect.TypeOf(u)
		// 如果是指针，获取其属性
		if typeOf.Kind() == reflect.Ptr {
			typeOf = typeOf.Elem()
		}
		field, ok := typeOf.FieldByName(fieldName) //通过反射获取filed
		if ok {
			errorInfo := field.Tag.Get("errorMsg") // 自定义解析tag errorMsg 的值
			if errorInfo == "" {
				setBuilder(&builder, validationErr.Error())
				continue
			}
			setBuilder(&builder, fmt.Sprint("Key: ", typeOf.Name(), ".", fieldName, " : ", errorInfo))
		}
	}
	if builder.Len() > 0 {
		return builder.String()
	}
	return err.Error()
}

func setBuilder(builder *strings.Builder, msg string) {
	if builder.Len() > 0 {
		builder.WriteString(" \n " + msg)
		return
	}
	builder.WriteString(msg)
}
