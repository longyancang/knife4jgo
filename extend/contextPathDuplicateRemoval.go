package extend

import (
	"gitee.com/longyancang/knife4jgo/openapi"
	"strings"
)

// ContextPathDuplicateRemoval http://localhost:8080/v3/api-docs 获取文档信息接口paths对象去掉跟路径
type ContextPathDuplicateRemoval interface {
	DuplicateRemoval(openApi *openapi.OpenAPI)
}

type duplicateRemoval struct {
	isEnable bool // 是否启用
}

var _ ContextPathDuplicateRemoval = &duplicateRemoval{}

func (d *duplicateRemoval) DuplicateRemoval(openApi *openapi.OpenAPI) {
	// 不启用直接返回
	if !d.isEnable {
		return
	}
	pathsCopy := make(map[string]*openapi.PathItem)
	for key, v := range openApi.Paths {
		all := strings.ReplaceAll(key, ExtendsRegisterDefault.Cp.GetContextPath(), "")
		pathsCopy[all] = v
	}
	openApi.Paths = pathsCopy
}
