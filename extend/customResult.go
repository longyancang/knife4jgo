package extend

import (
	"gitee.com/longyancang/knife4jgo/utils/result"
	"github.com/gin-gonic/gin"
	"net/http"
)

type CustomValidatorErrorRes interface {
	ValidatorErrorResFunc(c *gin.Context, e error) any
}

type validatorErrorRes struct {
	isEnable bool // 是否启用
}

var _ CustomValidatorErrorRes = &validatorErrorRes{}

func (r *validatorErrorRes) ValidatorErrorResFunc(c *gin.Context, e error) any {
	if !r.isEnable {
		return gin.H{
			"error": e.Error(),
		}
	}
	return result.NewErrorResult[string](http.StatusBadRequest, e.Error(), c)
}
