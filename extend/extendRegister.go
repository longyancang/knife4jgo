package extend

type ExtendsRegister struct {
	Cpdr ContextPathDuplicateRemoval // swagger查询api接口时，把根路径去掉，打开swagger页面时，url根路径会重复，如果没有重复可以不启用
	Vem  ValidatorErrorMessages      // 自定义Validator验证错误描述信息
	Cgwh CustomGinWrapHandler        // 自定义gin.handler包装器,默认为panic统一拦截器
	Acwh AssembleCustomWrapHandler   // 把每个gin.Handler包装一层自定义CustomGinWrapHandler
	Cver CustomValidatorErrorRes     // 自定义参数验证错误响应体,默认为自定义的Result结构体
	Cp   ContextPath                 // 根路径：http://localhost:8080/example/doc.html#  example为根路径
}

var ExtendsRegisterDefault ExtendsRegister

// ExtendsRegisterDefaultInit 扩展器初始化函数，可自定义
func ExtendsRegisterDefaultInit(rootPath string) {
	ExtendsRegisterDefault = ExtendsRegister{Cpdr: &duplicateRemoval{isEnable: true},
		Vem:  &errorMessages{isEnable: true},
		Cgwh: &customGinWrap{},
		Acwh: &assembleCustomWrap{isEnable: true},
		Cver: &validatorErrorRes{isEnable: true},
		Cp:   &serverContextPath{isEnable: true}}
	// 设置根路径
	ExtendsRegisterDefault.Cp.SetContextPath(rootPath)
}
