package extend

import (
	"strings"
)

type ContextPath interface {
	// SetContextPath 跟路径初始化函数
	SetContextPath(serverContextPathDefault string)
	// GetContextPath swagger 初始化时需要拼接根路径，调用此函数
	GetContextPath() string
}

type serverContextPath struct {
	isEnable    bool   // 是否启用
	ContextPath string // 服务根路径
}

var _ ContextPath = &serverContextPath{}

func (s *serverContextPath) SetContextPath(serverContextPathDefault string) {
	// 判断字符串是否以/开头
	if len(serverContextPathDefault) > 0 && !strings.HasPrefix(serverContextPathDefault, "/") {
		// 如果不是，则在字符串最前面加上/
		serverContextPathDefault = "/" + serverContextPathDefault
	}
	s.ContextPath = serverContextPathDefault
}

func (s *serverContextPath) GetContextPath() string {
	return s.ContextPath
}
