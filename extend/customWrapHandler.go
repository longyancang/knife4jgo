package extend

import (
	"fmt"
	"gitee.com/longyancang/knife4jgo/utils/result"
	"github.com/gin-gonic/gin"
	"net/http"
)

type CustomGinWrapHandler interface {
	CustomGinWrapFunc(handler gin.HandlerFunc) gin.HandlerFunc
}

type customGinWrap struct {
}

var _ CustomGinWrapHandler = &customGinWrap{}

// CustomGinWrapFunc 自定义panic统一拦截器
func (w *customGinWrap) CustomGinWrapFunc(handler gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 调用原始处理器
		defer func() {
			if err := recover(); err != nil {
				fmt.Println(c, err)
				var result1 result.Result[any]
				switch v := err.(type) {
				case result.Result[any]:
					result1 = v
				default:
					result1 = *result.NewErrorResult[any](http.StatusInternalServerError, fmt.Sprintf("%v", err), c)
				}
				c.JSON(http.StatusInternalServerError, result1)
			}
		}()
		handler(c)
	}
}

type AssembleCustomWrapHandler interface {
	AssembleCustomWrapHandlerFunc(handlers []gin.HandlerFunc) []gin.HandlerFunc
}

type assembleCustomWrap struct {
	isEnable bool // 是否启用
}

var _ AssembleCustomWrapHandler = &assembleCustomWrap{}

func (w *assembleCustomWrap) AssembleCustomWrapHandlerFunc(handlers []gin.HandlerFunc) []gin.HandlerFunc {
	// 不启用直接返回
	if !w.isEnable {
		return handlers
	}
	wrappedHandlers := make([]gin.HandlerFunc, len(handlers))
	for i, handler := range handlers {
		// 给每个handler，添加一层自定义CustomGinWrapHandler
		wrappedHandlers[i] = ExtendsRegisterDefault.Cgwh.CustomGinWrapFunc(handler)
	}
	return wrappedHandlers
}
