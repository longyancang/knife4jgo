package result

import (
	"gitee.com/longyancang/knife4jgo/constants"
	"github.com/gin-gonic/gin"
	"net/http"
)

// Result 定义统一返回对象
type Result[T any] struct {
	Code      int    `json:"code" description:"HTTP 状态码"`
	Message   string `json:"message,omitempty" description:"返回消息"`
	Data      T      `json:"data,omitempty" description:"返回的数据"`
	TheadName string `json:"theadName,omitempty" description:"链路追踪"`
}

type YdPage[T any] struct {
	Total   any `json:"total" description:"分页总数"`
	Records []T `json:"records" description:"分页数据"`
}

// NewYdPage 构造函数，用于创建并初始化 YdPage 对象
func NewYdPage[T any](total any, records []T) *YdPage[T] {
	if records == nil {
		records = make([]T, 0) // 如果 records 是 nil，则创建一个空的切片
	}
	return &YdPage[T]{
		Total:   total,
		Records: records,
	}
}

func NewOkResult[T any](data T, c *gin.Context) *Result[T] {
	return &Result[T]{
		Code:      http.StatusOK,
		Message:   "操作成功",
		Data:      data,
		TheadName: c.Request.Header.Get(constants.TraceId),
	}
}

func NewErrorResult[T any](code int, message string, c *gin.Context) *Result[T] {
	return &Result[T]{
		Code:      code,
		Message:   message,
		TheadName: c.Request.Header.Get(constants.TraceId),
	}
}

func NewErrorResultC[T any](code int, message string) *Result[T] {
	return &Result[T]{
		Code:    code,
		Message: message,
	}
}
